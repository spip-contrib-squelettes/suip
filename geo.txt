
https://geoservices.ign.fr/documentation/services/utilisation-web/affichage-wmts/leaflet-et-wmts

<style type="text/css">
   #viewerDiv {
       width:100%;
       height:600px;
   }
</style>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
<div id="viewerDiv"></div>
<script type="text/javascript">
    window.onload= function() {
        var map = L.map("viewerDiv").setView([48.845,2.424],10) ;
        L.tileLayer(
            'https://wxs.ign.fr/CLEF/geoportail/wmts?service=WMTS&request=GetTile&version=1.0.0&tilematrixset=PM&tilematrix={z}&tilecol={x}&tilerow={y}&layer=ORTHOIMAGERY.ORTHOPHOTOS&format=image/jpeg&style=normal',
            {
                minZoom : 0,
                maxZoom : 18,
                tileSize : 256,
                attribution : "IGN-F/Géoportail"
            }).addTo(map);
    }
</script>

=> dans /config/mes_options.php (pour les cartes 1/25000ème:

<?php
// 
$GLOBALS['gis_layers']['geoign'] = array(
	'nom' => 'Geoportail IGN',
	'layer' => 'L.tileLayer(
            "https://wxs.ign.fr/CLEF/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
            {
                minZoom : 0,
                maxZoom : 18,
                tileSize : 256,
                attribution : "IGN-F/Géoportail"
            })',
);
?>
