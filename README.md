# suip

Jeu de squelettes basé sur css semantic ui . 
Objectifs : géolocalisation objets (rubrique, album,…) création de topo-carte web svg créative commons .


Un jeu de squelettes prêt à l'emploi (sauf erreurs à corriger ?…) absolument pas paramétrable depuis l’espace privé, 
mais en modifiant le code html, avec une documentation détaillée permettant une adaptation facile à vos goûts, couleurs et besoins.

CSS & JS
========
La base css est semantic ui * . Les icônes : css/themes/icons.svg .woff pourrait sans doute être modifié, y a même pas d'icône spip !
semantic.js n'est pas utilisé dans le jeu de squelettes de base mais permet si besoin des effets trés fun (sidebar, etc.).

ALBUMS
======
Prévu pour acceuillir des albums (photos, etc.) classés par mots clés. Utilise albums-3. 

TOPO GÉO
========
La création et l'évolution de topo-carte web créative commons en svg en s'appuyant sur localisation et trace gps était l'objectif premier,  
à l'origine pour des topos d'escalade de blocs, cela peut sans doute répondre à d'autres besoins : randonnées, courses d'orientation, …  
⋅GÉO : Les pages gisX json/gis_X et topo.html sont a modifier en fonction des objets et mots clés servant de légende sur les cartes.
⋅TOPO : Il est possible avec du calcul matriciel somme toute assez basic d'utiliser des relevés gps sur un topo-carte svg (et réciproquement),
la création de, puis la contribution au topo-carte svg en est grandement facilité. 
Les topos svg nécessitent svgpanzoom pour zoomer et semantic.js pour les popups. 
N'utilise pas jQuery Vector Maps mais ce serait sans doute à creuser.

REVISION
========
Les révisions montrées côté public sont uniquement les révisions du texte. C'est assurément un manque à combler ! 
Page revisions.html avec un s . Il ne faut pas créer de page revision.html car sinon interférence avec la page et le cache de l'espace privé. De même et pour la même raison il ne doit pas y avoir de page document.html, d'où la page docu.html et le lien sur cette ci depuis revision d'un document. 

----------------------------------------------------------

* https://semantic-ui.com
  https://semantic-ui.mit-license.org/ : 
«

 The MIT License (MIT)

Copyright © 2022 , copyright holders .

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

»

traduction : 

 La licence MIT (MIT)

Copyright © 2022 , titulaires des droits d'auteur .

L'autorisation est accordée, gratuitement, à toute personne obtenant une copie de ce logiciel et des fichiers de documentation associés (le "Logiciel"), de traiter le Logiciel sans restriction, y compris, sans limitation, les droits d'utilisation, de copie, de modification, de fusion , publier, distribuer, sous-licencier et/ou vendre des copies du Logiciel, et permettre aux personnes à qui le Logiciel est fourni de le faire, sous réserve des conditions suivantes :

L'avis de droit d'auteur ci-dessus et cet avis d'autorisation doivent être inclus dans toutes les copies ou parties substantielles du Logiciel.

LE LOGICIEL EST FOURNI « EN L'ÉTAT », SANS GARANTIE D'AUCUNE SORTE, EXPLICITE OU IMPLICITE, Y COMPRIS, MAIS SANS S'Y LIMITER, LES GARANTIES DE QUALITÉ MARCHANDE, D'ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN CAS, LES AUTEURS OU LES DÉTENTEURS DU COPYRIGHT NE SERONT RESPONSABLES DE TOUTE RÉCLAMATION, DOMMAGE OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS UNE ACTION CONTRACTUELLE, DÉLICTUELLE OU AUTRE, DÉCOULANT DE, DE OU EN RELATION AVEC LE LOGICIEL OU L'UTILISATION OU D'AUTRES TRANSACTIONS DANS LE LOGICIEL.

