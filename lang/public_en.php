<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://trac.rezo.net/spip/spip/ecrire/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'revision' => 'revisions',
	'lien_connecter' => 'Log in',
	'icone_deconnecter' => 'Log out',
	'info_langues' => '<i class="gb flag"></i>',
	'ajouter_albumdoc' => 'You can add pics now or later when the album is created.',
	'participer_site' => 'You can take active part in this website, upload photos, new albums, topos, articles, making corrections, etc. contact an active member.',
	'blabla' => ', bla, blah.',
	'der_albums' => 'last albums',
	'der_avis' => 'last estimation',
	'der_photos' => 'last pictures',
	'der_videos' => 'last videos',
	'affinez' => 'can be refine your search ...',
	'droit_auteur' => 'Unless otherwise specified, ©opyleft',
	'bouton_afficher' => 'display',
);
?>
