<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://trac.rezo.net/spip/spip/ecrire/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'revision' => 'revisions',
	'lien_connecter' => 'Log in',
	'icone_deconnecter' => 'Log out',
	'info_langues' => '<i class="fr flag"></i>',
	'ajouter_albumdoc' => 'Vous pouvez ajouter des photos tout de suite, ou plus tard une fois l’album créé.',
	'participer_site' => 'Pour participer à ce site, ajouter des photos, albums, topos, nouveaux articles, effectuer des corrections, etc. prenez contact avec un membre actif.',
	'blabla' => 'blabla',
	'der_albums' => 'derniers albums',
	'der_avis' => 'der répét’ et éval’',
	'der_photos' => 'dernières photos',
	'der_videos' => 'dernières videos',
	'affinez' => 'Reformuler cette recherche peut être ?',
	'droit_auteur' => 'Droit d\'auteur, sauf mention contraire :',
	'bouton_afficher' => 'Afficher',
);
?>
